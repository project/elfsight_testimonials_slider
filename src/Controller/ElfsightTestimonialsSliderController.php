<?php

namespace Drupal\elfsight_testimonials_slider\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightTestimonialsSliderController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/testimonials-slider/?utm_source=portals&utm_medium=drupal&utm_campaign=testimonials-slider&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
